#!/bin/bash -l

#SBATCH -p debug
#SBATCH -N 1
#SBATCH -t 00:10:00
#SBATCH -J hpgmg-fe-sr

# Variables
JOBID=${SLURM_JOBID}
NODES=${SLURM_JOB_NUM_NODES}
CORES=24*SLURM_JOB_NUM_NODES
PRE=3
POST=1
SAMPLES=8
S=0
Q=0
echo "N =" ${NODES}
echo "n =" ${CORES}
echo "Nodes:" ${SLURM_JOB_NODELIST}
echo "JobId:" ${JOBID}

date
mp=32
p=1
((m=mp*2))
((lmin=mp*mp*mp))
((lmax=lmin+1))
srun -n ${p} ./build/bin/hpgmg-fe sample -repeat ${SAMPLES} -op_type poisson1 -smooth ${PRE},${POST} -local ${lmin},${lmax} -sr_levels ${S} -sr_max_buf ${Q} -log_summary >& n00000${p}_mp${mp}_S${S}.Q${Q}.$JOBID.out

date
mp=32
p=8
((m=mp*2))
((lmin=mp*mp*mp))
((lmax=lmin+1))
srun -n ${p} ./build/bin/hpgmg-fe sample -repeat ${SAMPLES} -op_type poisson1 -smooth ${PRE},${POST} -local ${lmin},${lmax} -sr_levels ${S} -sr_max_buf ${Q} -log_summary >& n00000${p}_mp${mp}_S${S}.Q${Q}.$JOBID.out

date
mp=32
p=64
((m=mp*4))
((lmin=mp*mp*mp))
((lmax=lmin+1))
#srun -n ${p} ./build/bin/hpgmg-fe sample -repeat ${SAMPLES} -op_type poisson1 -smooth ${PRE},${POST} -local ${lmin},${lmax} -sr_levels ${S} -sr_max_buf ${Q} -log_summary >& n0000${p}_mp${mp}_S${S}.Q${Q}.$JOBID.out

date
mp=32
p=512
((m=mp*8))
((lmin=mp*mp*mp))
((lmax=lmin+1))
#srun -n ${p} ./build/bin/hpgmg-fe sample -repeat ${SAMPLES} -op_type poisson1 -smooth ${PRE},${POST} -local ${lmin},${lmax} -sr_levels ${S} -sr_max_buf ${Q} -log_summary >& n000${p}_mp${mp}_S${S}.Q${Q}.$JOBID.out

date
# eof
