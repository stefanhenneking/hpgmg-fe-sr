#include "fefas.h"
#include "op/fefas-op.h"
#include <petscksp.h>

typedef struct Options_private *Options;
struct Options_private {
  PetscInt M[3];
  PetscInt p[3];
  PetscInt cmax;
  PetscReal L[3];
  PetscInt  smooth[2];
  PetscBool coord_distort;
  PetscInt sr_levels; // Number of SR levels
  PetscInt sr_max_buf; // Maximum buffer schedule: sr_max_buf*(2^sr_level)
};

struct MG_private {
  DM dm;
  KSP ksp;
  MG coarse;
  PetscReal enormInfty,enormL2;
  PetscReal rnorm2,bnorm2;      // rnorm is normalized by bnorm
  PetscBool monitor;
  PetscBool diagnostics;
  PetscLogStage stage;
  PetscLogEvent V_Cycle;
};

static PetscErrorCode OptionsParse(const char *header,Options *opt)
{
  PetscErrorCode ierr;
  Options o;
  PetscInt three,two,M_max;

  PetscFunctionBegin;
  *opt = NULL;
  ierr = PetscNew(&o);CHKERRQ(ierr);
  o->M[0] = 10;
  o->M[1] = 10;
  o->M[2] = 10;
  o->p[0] = 1;
  o->p[1] = 1;
  o->p[2] = 1;
  // cmax is an annoyingly tricky parameter.  The smallest coarse grid we want to deal with is 3x4x4=48 elements, which
  // corresponds to 7x9x9=567 Q2 finite-element nodes.  That coarse grid size is critical because a non-multigrid method
  // needs to be able to solve fast there (and everyone is waiting for it).  Given communication latency in the
  // microsecond range, one refinement (6x8x8 elements) is sufficient to start distributing, so cmax should be smaller
  // than 3x4x4*2^3=384.
  //
  // Meanwhile, we want to support process grids like 1x1x3 for cases where the total number of processes has such a
  // factor.  The coarsening semantic requires that a 2x2x2 grid resides one process, so the grid sizes are 4x4x4 on
  // 1x1x2 and 8x8x8 on 1x1x3.  In the latter case, each process in the 1x1x3 grid will own 8*8*ceil(8/3)=192 elements.
  // We intend to ensure that no process grids be more anisotropic than 1x1x3.
  o->cmax = 192;
  ierr = PetscOptionsBegin(PETSC_COMM_WORLD,NULL,header,NULL);CHKERRQ(ierr);
  three = 3;
  ierr = PetscOptionsIntArray("-M","Fine grid dimensions","",o->M,&three,NULL);CHKERRQ(ierr);
  three = 3;
  ierr = PetscOptionsIntArray("-p","Process grid dimensions","",o->p,&three,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-cmax","Max coarse grid size","",o->cmax,&o->cmax,NULL);CHKERRQ(ierr);
  M_max = PetscMax(o->M[0],PetscMax(o->M[1],o->M[2]));
  o->L[0] = o->M[0]*1./M_max;
  o->L[1] = o->M[1]*1./M_max;
  o->L[2] = o->M[2]*1./M_max;
  three = 3;
  ierr = PetscOptionsRealArray("-L","Grid dimensions","",o->L,&three,NULL);CHKERRQ(ierr);
  o->smooth[0] = 3;
  o->smooth[1] = 1;
  two = 2;
  ierr = PetscOptionsIntArray("-smooth","V- and F-cycle pre,post smoothing","",o->smooth,&two,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-coord_distort","Distort coordinates within unit cube","",o->coord_distort,&o->coord_distort,NULL);CHKERRQ(ierr);
  // Segmental Refinement Parameter
  o->sr_levels = 0;
  o->sr_max_buf = 0;
  ierr = PetscOptionsInt("-sr_levels","Number of segmental refinement levels","",o->sr_levels,&o->sr_levels,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-sr_max_buf","Size of maximum buffer on transition level","",o->sr_max_buf,&o->sr_max_buf,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);
  *opt = o;
  PetscFunctionReturn(0);
}

PetscErrorCode MGMonitorSet(MG mg,PetscBool mon) {
  for ( ; mg; mg=mg->coarse) mg->monitor = mon;
  return 0;
}

PetscErrorCode MGCreate(Op op,DM dm,PetscInt nlevels,MG *newmg) {
  PetscErrorCode ierr;
  MG mg;
  PetscInt two, sr_level;
  PetscReal eig_target[2];
  PetscBool monitor;

  PetscFunctionBegin;
  ierr = PetscOptionsBegin(PetscObjectComm((PetscObject)dm),NULL,"MG Options",NULL);CHKERRQ(ierr);
  two = 2;
  eig_target[0] = 1.4;
  eig_target[1] = 0.4;
  ierr = PetscOptionsRealArray("-mg_eig_target","Target max,min eigenvalues on levels","",eig_target,&two,NULL);CHKERRQ(ierr);
  monitor = PETSC_FALSE;
  ierr = PetscOptionsBool("-mg_monitor","Monitor convergence at the end of each MG cycle","",monitor,&monitor,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);
  ierr = DMGetSRLevel(dm,&sr_level);CHKERRQ(ierr);
  ierr = PetscNew(&mg);CHKERRQ(ierr);
  mg->dm = dm;
  *newmg = mg;
  // A solver context and preconditioner is created for every level
  for (PetscInt lev=nlevels-1; ; lev--, sr_level--) {
    DM dmcoarse;
    if (mg->dm) { // I have some grid at this level
      Mat A;
      PC pc;
      char prefix[256];
      // Set solver context depending on SR / non-SR
      if (sr_level>0) {ierr = KSPCreate(PETSC_COMM_SELF,&mg->ksp);CHKERRQ(ierr);}
      else            {ierr = KSPCreate(PetscObjectComm((PetscObject)mg->dm),&mg->ksp);CHKERRQ(ierr);}
      
      if (lev) {
        // Chebyshev smoother on normal levels
        ierr = KSPSetConvergenceTest(mg->ksp,KSPConvergedSkip,NULL,NULL);CHKERRQ(ierr);
        ierr = KSPSetNormType(mg->ksp,KSP_NORM_NONE);CHKERRQ(ierr);
        ierr = KSPSetType(mg->ksp,KSPCHEBYSHEV);CHKERRQ(ierr);
      } else {
        // CG on coarsest level
        ierr = KSPSetNormType(mg->ksp,KSP_NORM_NATURAL);CHKERRQ(ierr);
        ierr = KSPSetType(mg->ksp,KSPCG);CHKERRQ(ierr);
        ierr = KSPSetTolerances(mg->ksp,1e-10,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);
      }
      ierr = KSPChebyshevSetEigenvalues(mg->ksp,eig_target[0],eig_target[1]);CHKERRQ(ierr);
      ierr = KSPSetInitialGuessNonzero(mg->ksp,PETSC_TRUE);CHKERRQ(ierr);
      ierr = KSPGetPC(mg->ksp,&pc);CHKERRQ(ierr);
      ierr = PCSetType(pc,PCJACOBI);CHKERRQ(ierr); // Set preconditioner: diagonal (jacobi)
      // This is where we have to get different "matrices" on SR level
      if (sr_level>0) {ierr = OpGetMatSR(op,mg->dm,&A);CHKERRQ(ierr);} // SR
      else            {ierr = OpGetMat  (op,mg->dm,&A);CHKERRQ(ierr);} // Non-SR
      // KSPSetOperators(ksp,amat,pmat): pmat is matrix from which the preconditioner is constructed
      // A is placeholder (shell) if matrix-free method (OpGetMat: MatCreateShell())
      ierr = KSPSetOperators(mg->ksp,A,A);CHKERRQ(ierr);
      ierr = MatDestroy(&A);CHKERRQ(ierr);
      ierr = PetscSNPrintf(prefix,sizeof prefix,"mg_%D_",lev);CHKERRQ(ierr);
      ierr = KSPSetOptionsPrefix(mg->ksp,prefix);CHKERRQ(ierr);
      ierr = KSPSetFromOptions(mg->ksp);CHKERRQ(ierr);
    }
    if (lev == 0) break;
    if (mg->dm) {
      ierr = DMFECoarsen(mg->dm,&dmcoarse);CHKERRQ(ierr);
    } else dmcoarse = NULL;
    MPI_Barrier(PETSC_COMM_WORLD);
    ierr = PetscNew(&mg->coarse);CHKERRQ(ierr);
    mg->coarse->dm = dmcoarse;
    mg = mg->coarse;
  }
  if (monitor) {ierr = MGMonitorSet(*newmg,PETSC_TRUE);CHKERRQ(ierr);}
  PetscFunctionReturn(0);
}

PetscErrorCode MGDestroy(MG *mg) {
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if (!*mg) PetscFunctionReturn(0);
  ierr = MGDestroy(&(*mg)->coarse);CHKERRQ(ierr);
  ierr = KSPDestroy(&(*mg)->ksp);CHKERRQ(ierr);
  ierr = PetscFree(*mg);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

// PCJacobi does not extract the diagonal until it is applied to a vector.
PetscErrorCode MGSetUpPC(MG mg) {
  PetscErrorCode ierr;
  PetscInt sr_level;
  
  PetscFunctionBegin;
  ierr = DMGetSRLevel(mg->dm,&sr_level);CHKERRQ(ierr);
  for (; mg; mg=mg->coarse, sr_level--) {
    if (mg->dm) {
      PC pc;
      Vec U,V;
      ierr = KSPGetPC(mg->ksp,&pc);CHKERRQ(ierr);
      if (sr_level>0) {
        ierr = DMGetLocalVector(mg->dm,&U);CHKERRQ(ierr);
        ierr = DMGetLocalVector(mg->dm,&V);CHKERRQ(ierr);
      } else {
        ierr = DMGetGlobalVector(mg->dm,&U);CHKERRQ(ierr);
        ierr = DMGetGlobalVector(mg->dm,&V);CHKERRQ(ierr);
      }
      ierr = VecZeroEntries(U);CHKERRQ(ierr);
      ierr = PCApply(pc,U,V);CHKERRQ(ierr);
      if (sr_level>0) {
        ierr = DMRestoreLocalVector(mg->dm,&U);CHKERRQ(ierr);
        ierr = DMRestoreLocalVector(mg->dm,&V);CHKERRQ(ierr);
      } else {
        ierr = DMRestoreGlobalVector(mg->dm,&U);CHKERRQ(ierr);
        ierr = DMRestoreGlobalVector(mg->dm,&V);CHKERRQ(ierr);
      }
    }
  }
  PetscFunctionReturn(0);
}

static PetscReal ConvergenceRate(PetscReal normCoarse,PetscReal normFine) {
  // Try to avoid reporting noisy rates in pre-asymptotic regime
  if (normCoarse < 1e3*PETSC_MACHINE_EPSILON && normFine > 1e3*PETSC_MACHINE_EPSILON) return 0;
  if (normCoarse == 0 || normFine == 0) return 0;
  return log2(normCoarse/normFine);
}

static PetscErrorCode MGSetProfiling(MG mg,PetscBool flg) {
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if (flg) {
    PetscInt lev;
    ierr = DMFEGetInfo(mg->dm,NULL,&lev,NULL,NULL,NULL);CHKERRQ(ierr);
    for ( ; lev >= 0; mg=mg->coarse,lev--) {
      char name[256];
      ierr = PetscSNPrintf(name,sizeof name,"FMG Level %D",lev);CHKERRQ(ierr);
      ierr = PetscLogStageRegister(name,&mg->stage);CHKERRQ(ierr);
      ierr = PetscSNPrintf(name,sizeof name,"MGVcycleLevel%D",lev);CHKERRQ(ierr);
      ierr = PetscLogEventRegister(name,DM_CLASSID,&mg->V_Cycle);CHKERRQ(ierr);
    }
  }
  PetscFunctionReturn(0);
}

static PetscErrorCode MGRecordDiagnostics(Op op,MG mg,Vec B,Vec U) {
  PetscErrorCode ierr;
  Vec Y;
  PetscInt sr_level, sr_buf;

  PetscFunctionBegin;
  if (mg->monitor || mg->diagnostics) {
    ierr = DMGetGlobalVector(mg->dm,&Y);CHKERRQ(ierr);
    ierr = OpApply(op,mg->dm,U,Y);CHKERRQ(ierr);
    ierr = VecAYPX(Y,-1.,B);CHKERRQ(ierr);
    ierr = VecNorm(Y,NORM_2,&mg->rnorm2);CHKERRQ(ierr);
    if (mg->bnorm2 > 1e3*PETSC_MACHINE_EPSILON && mg->rnorm2 > 1e3*PETSC_MACHINE_EPSILON) mg->rnorm2 /= mg->bnorm2;
    ierr = DMRestoreGlobalVector(mg->dm,&Y);CHKERRQ(ierr);

    ierr = OpIntegrateNorms(op,mg->dm,U,&mg->enormInfty,&mg->enormL2);CHKERRQ(ierr);
  }
  if (mg->monitor) {
    PetscInt fedegree,level,mlocal[3],Mglobal[3],procs[3];
    PetscReal erateInfty = 0,erateL2 = 0,rrate2 = 0;
    ierr = DMFEGetInfo(mg->dm,&fedegree,&level,mlocal,Mglobal,procs);CHKERRQ(ierr);
    if (mg->coarse) {
      erateInfty = ConvergenceRate(mg->coarse->enormInfty,mg->enormInfty);
      erateL2    = ConvergenceRate(mg->coarse->enormL2,mg->enormL2);
      rrate2     = ConvergenceRate(mg->coarse->rnorm2,mg->rnorm2);
    }
    ierr = DMGetSRLevel(mg->dm,&sr_level);CHKERRQ(ierr);
    ierr = DMGetSRBufferSize(mg->dm,&sr_buf);CHKERRQ(ierr);
    char sr[32]; sr_level>0 ? sprintf(sr," [SR], buf=") : sprintf(sr,"\n");
    ierr = PetscPrintf(PetscObjectComm((PetscObject)mg->dm),"Q%D %2D e_max %8.2e(%4.1f) e_L2 %8.2e(%4.1f) r_2 %8.2e(%4.1f) G[%5D%5D%5D] L[%4D%4D%4D] P[%3D%3D%3D]%s",
                       fedegree,level,
                       mg->enormInfty,erateInfty,    // normalized by exact solution
                       mg->enormL2,erateL2,          // normalized by exact solution
                       mg->rnorm2,rrate2,            // algebraic, normalized by (non-tau-corrected) algebraic forcing
                       Mglobal[0],Mglobal[1],Mglobal[2],
                       mlocal[0],mlocal[1],mlocal[2],
                       procs[0],procs[1],procs[2],sr,sr_buf);CHKERRQ(ierr);
    if (sr_level>0) {
      ierr = PetscPrintf(PetscObjectComm((PetscObject)mg->dm),"%D\n",sr_buf);CHKERRQ(ierr);
    }
  }
  PetscFunctionReturn(0);
}

// Written in FAS form
//   Ac uc = R bf + Ac Rhat uf - R Af uf
// Collecting the affine terms:
//   Ac uc = R (bf - Af uf) + Ac Rhat uf
static PetscErrorCode MGVCycle(Op op,MG mg,PetscInt presmooths,PetscInt postsmooths,Vec B,Vec U) {
  PetscErrorCode ierr;
  DM dm = mg->dm,dmcoarse;
  Vec V,Vc,Uc;

  PetscFunctionBegin;
  ierr = PetscLogEventBegin(mg->V_Cycle,mg->dm,B,U,0);CHKERRQ(ierr);
  if (presmooths) {
    ierr = KSPSetTolerances(mg->ksp,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT,mg->coarse?presmooths-1:20);CHKERRQ(ierr);
    ierr = KSPSolve(mg->ksp,B,U);CHKERRQ(ierr);
  }
  if (!mg->coarse) PetscFunctionReturn(0);
  dmcoarse = mg->coarse->dm;
  ierr = DMGetGlobalVector(dm,&V);CHKERRQ(ierr);
  ierr = OpApply(op,dm,U,V);CHKERRQ(ierr);
  ierr = VecAYPX(V,-1,B);CHKERRQ(ierr);
  if (dmcoarse) {
    ierr = DMGetGlobalVector(dmcoarse,&Uc);CHKERRQ(ierr);
    ierr = DMGetGlobalVector(dmcoarse,&Vc);CHKERRQ(ierr);
  } else {
    Uc = NULL;
    Vc = NULL;
  }
  ierr = OpRestrictState(op,dm,U,Uc);CHKERRQ(ierr);
  if (dmcoarse) {
    ierr = OpApply(op,dmcoarse,Uc,Vc);CHKERRQ(ierr);
  }
  ierr = OpRestrictResidual(op,dm,V,Vc);CHKERRQ(ierr);
  if (dmcoarse) {
    Vec Yc;
    ierr = DMFEZeroBoundaries(dmcoarse,Vc);CHKERRQ(ierr);
    ierr = DMGetGlobalVector(dmcoarse,&Yc);CHKERRQ(ierr);
    ierr = VecCopy(Uc,Yc);CHKERRQ(ierr);
    ierr = MGVCycle(op,mg->coarse,presmooths,postsmooths,Vc,Uc);CHKERRQ(ierr);
    ierr = VecAXPY(Uc,-1.,Yc);CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(dmcoarse,&Yc);CHKERRQ(ierr);
  }
  ierr = OpInterpolate(op,dm,Uc,V);CHKERRQ(ierr);
  ierr = VecAXPY(U,1,V);CHKERRQ(ierr);
  if (dmcoarse) {
    ierr = DMRestoreGlobalVector(dmcoarse,&Uc);CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(dmcoarse,&Vc);CHKERRQ(ierr);
  }
  ierr = DMRestoreGlobalVector(dm,&V);CHKERRQ(ierr);
  if (postsmooths) {
    ierr = KSPSetTolerances(mg->ksp,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT,postsmooths-1);CHKERRQ(ierr);
    ierr = KSPSolve(mg->ksp,B,U);CHKERRQ(ierr);
  }
  ierr = PetscLogEventEnd(mg->V_Cycle,mg->dm,B,U,0);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode MGFCycle(Op op,MG mg,PetscInt presmooths,PetscInt postsmooths,Vec B,Vec U) {
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscLogStagePush(mg->stage);CHKERRQ(ierr);
  if (mg->monitor || mg->diagnostics) {
    ierr = VecNorm(B,NORM_2,&mg->bnorm2);CHKERRQ(ierr);
  }
  if (mg->coarse) {
    Vec Uc,Bc;
    DM dmcoarse = mg->coarse->dm;
    if (dmcoarse) {
      ierr = DMGetGlobalVector(dmcoarse,&Bc);CHKERRQ(ierr);
      ierr = VecZeroEntries(Bc);CHKERRQ(ierr);
    } else Bc = NULL;
    ierr = OpRestrictResidual(op,mg->dm,B,Bc);CHKERRQ(ierr);
    if (dmcoarse) {
      ierr = DMFEZeroBoundaries(dmcoarse,Bc);CHKERRQ(ierr);
      ierr = DMGetGlobalVector(dmcoarse,&Uc);CHKERRQ(ierr);
      ierr = MGFCycle(op,mg->coarse,presmooths,postsmooths,Bc,Uc);CHKERRQ(ierr);
    } else Uc = NULL;
    ierr = OpInterpolate(op,mg->dm,Uc,U);CHKERRQ(ierr);
    if (dmcoarse) {
      ierr = DMRestoreGlobalVector(dmcoarse,&Uc);CHKERRQ(ierr);
      ierr = DMRestoreGlobalVector(dmcoarse,&Bc);CHKERRQ(ierr);
    }
  }
  ierr = MGVCycle(op,mg,presmooths,postsmooths,B,U);CHKERRQ(ierr);
  ierr = PetscLogStagePop();CHKERRQ(ierr);
  ierr = MGRecordDiagnostics(op,mg,B,U);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode RunMGV()
{
  PetscErrorCode ierr;
  Grid grid;
  Options opt;
  Op op;
  PetscInt fedegree,dof,nlevels;
  DM dm;
  Vec U0,U,F;
  MG mg;
  PetscReal normU0,normError,normF,normResid;

  PetscFunctionBegin;
  ierr = OpCreateFromOptions(PETSC_COMM_WORLD,&op);CHKERRQ(ierr);
  ierr = OpGetFEDegree(op,&fedegree);CHKERRQ(ierr);
  ierr = OpGetDof(op,&dof);CHKERRQ(ierr);
  ierr = OptionsParse("Finite Element FAS FMG solver",&opt);CHKERRQ(ierr);
  if (opt->sr_levels || opt->sr_max_buf) {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"RunMGV called with segmental refinement");
  }
  ierr = GridCreate(PETSC_COMM_WORLD,opt->M,opt->p,opt->cmax,0,0,&grid);CHKERRQ(ierr);
  ierr = GridGetNumLevels(grid,&nlevels);CHKERRQ(ierr);

  ierr = GridView(grid);CHKERRQ(ierr);
  ierr = DMCreateFE(grid,fedegree,dof,&dm);CHKERRQ(ierr);
  ierr = DMFESetUniformCoordinates(dm,opt->L);CHKERRQ(ierr);

  ierr = DMCreateGlobalVector(dm,&U0);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm,&U);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm,&F);CHKERRQ(ierr);
  ierr = OpForcing(op,dm,F);CHKERRQ(ierr);
  ierr = OpSolution(op,dm,U0);CHKERRQ(ierr);
  ierr = VecNorm(U0,NORM_2,&normU0);CHKERRQ(ierr);
  ierr = VecNorm(F,NORM_2,&normF);CHKERRQ(ierr);

  ierr = MGCreate(op,dm,nlevels,&mg);CHKERRQ(ierr);
  ierr = MGSetProfiling(mg,PETSC_TRUE);CHKERRQ(ierr);
  for (PetscInt i=0; i<5; i++) {
    Vec Y;
    ierr = MGVCycle(op,mg,opt->smooth[0],opt->smooth[1],F,U);CHKERRQ(ierr);
    ierr = DMGetGlobalVector(dm,&Y);CHKERRQ(ierr);
    ierr = VecCopy(U,Y);CHKERRQ(ierr);
    ierr = VecAXPY(Y,-1,U0);CHKERRQ(ierr);
    ierr = VecNorm(Y,NORM_2,&normError);CHKERRQ(ierr);
    ierr = OpApply(op,dm,U,Y);CHKERRQ(ierr);
    ierr = VecAYPX(Y,-1,F);CHKERRQ(ierr);
    ierr = VecNorm(Y,NORM_2,&normResid);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_WORLD,"V(%D,%D) %2D: |e|_2/|u|_2 %8.2e  |r|_2/|f|_2 %8.2e\n",opt->smooth[0],opt->smooth[1],i,(double)(normError/normU0),(double)(normResid/normF));CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(dm,&Y);CHKERRQ(ierr);
  }

  ierr = MGDestroy(&mg);CHKERRQ(ierr);
  ierr = VecDestroy(&U0);CHKERRQ(ierr);
  ierr = VecDestroy(&U);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  ierr = GridDestroy(&grid);CHKERRQ(ierr);
  ierr = OpDestroy(&op);CHKERRQ(ierr);
  ierr = PetscFree(opt);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

// Smooth 5% mesh distortion implemented by "swirling" the region inside |(x,y)|_2 < 1.
PetscErrorCode DMCoordDistort(DM dm,const PetscReal L[]) {
  PetscErrorCode ierr;
  PetscScalar *xx;
  PetscInt m;
  Vec X;

  PetscFunctionBegin;
  ierr = DMGetCoordinatesLocal(dm,&X);CHKERRQ(ierr);
  ierr = VecGetLocalSize(X,&m);CHKERRQ(ierr);
  ierr = VecGetArray(X,&xx);CHKERRQ(ierr);
  for (PetscInt i=0; i<m; i+=3) {
    PetscReal x = xx[i]/L[0],y = xx[i+1]/L[1],z = xx[i+2]/L[2],r2,theta,newx,newy;
    r2 = PetscMin(PetscSqrtReal(PetscSqr(2*x-1) + PetscSqr(2*y-1)),1);
    theta = 0.1*PetscSqr(PetscCosReal(PETSC_PI*r2/2)) * PetscSinReal(PETSC_PI*z);
    newx = (.5 + PetscCosReal(theta)*(x-.5) - PetscSinReal(theta)*(y-.5))*L[0];
    newy = (.5 + PetscSinReal(theta)*(x-.5) + PetscCosReal(theta)*(y-.5))*L[1];
    if (0) printf("%6.3f %6.3f %6.3f:  r2 %6.3f  theta %6.3f  dx %6.3f %6.3f\n",xx[i+0],xx[i+1],xx[i+2],r2,theta,newx-xx[i],newy-xx[i+1]);
    xx[i+0] = newx;
    xx[i+1] = newy;
  }
  ierr = VecRestoreArray(X,&xx);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

// Written in FAS form
//   Ac uc = R bf + Ac Rhat uf - R Af uf
// Collecting the affine terms:
//   Ac uc = R (bf - Af uf) + Ac Rhat uf
//
// Input are LOCAL Vectors B and U
static PetscErrorCode MGVCycleSR(Op op,MG mg,PetscInt presmooths,PetscInt postsmooths,Vec B,Vec U) {
  PetscErrorCode ierr;
  DM dm = mg->dm,dmcoarse;
  Vec V,Vc,Uc;
  PetscInt sr_level;
  
  PetscFunctionBegin;
  
  ierr = DMGetSRLevel(dm,&sr_level);CHKERRQ(ierr);
  PetscBool TL = (sr_level==1); // Is the next coarser level the transition level?
  if (!(sr_level>0)) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"MGVCycleSR called on a non-SR level");
  if (!(mg->coarse)) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"MGVCycleSR cannot compute coarsest level");
  dmcoarse = mg->coarse->dm;
  if (!(dmcoarse) && !TL) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"MGVCycleSR: no process grid coarsening on SR levels");

  ierr = PetscLogEventBegin(mg->V_Cycle,mg->dm,B,U,0);CHKERRQ(ierr);
  
  if (presmooths) {
    ierr = KSPSetTolerances(mg->ksp,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT,mg->coarse?presmooths-1:20);CHKERRQ(ierr);
    ierr = KSPSolve(mg->ksp,B,U);CHKERRQ(ierr);
  }
  
  ierr = DMGetLocalVector(dm,&V);CHKERRQ(ierr);
  ierr = OpApply_Poisson1_SR(op,dm,U,V);CHKERRQ(ierr); // V is now the LHS approximation (V=LU) of discretized PDE
  ierr = VecAYPX(V,-1,B);CHKERRQ(ierr); // V=B-V, or V=B-LU; B is RHS (forcing function F)--> V is residual now
  
  if (dmcoarse) {
    if(!TL) {
      ierr = DMGetLocalVector(dmcoarse,&Vc);CHKERRQ(ierr);
      ierr = DMGetLocalVector(dmcoarse,&Uc);CHKERRQ(ierr);
    } else {
      ierr = DMGetGlobalVector(dmcoarse,&Vc);CHKERRQ(ierr);
      ierr = DMGetGlobalVector(dmcoarse,&Uc);CHKERRQ(ierr);
    }
  } else {
    Vc = NULL;
    Uc = NULL;
  }
  // Injection of fine grid solution into coarse grid
  if (!TL) {ierr = DMFERestrictStateSR(dm,U,Uc);CHKERRQ(ierr);}
  else     {ierr = DMFERestrictStateTL(dm,U,Uc);CHKERRQ(ierr);}
  
  // Compute coarse grid left-hand side (Vc=LUc)
  if (dmcoarse) {
    if (!TL) {ierr = OpApply_Poisson1_SR(op,dmcoarse,Uc,Vc);CHKERRQ(ierr);}
    else     {ierr = OpApply            (op,dmcoarse,Uc,Vc);CHKERRQ(ierr);}
  }
  
  // Restriction of residual: Vc+=R(V), i.e., Vc=L(Rhat U)+R(B-LU)
  if (!TL) {ierr = DMFERestrictSR       (dm,V,Vc);CHKERRQ(ierr);}
  else     {ierr = OpRestrictResidual(op,dm,V,Vc);CHKERRQ(ierr);} // Can do this w/o communication
  
  if (dmcoarse) {
    Vec Yc;
    // Set global boundaries of coarse grid RHS to zero, SR: Set frozen nodes zero
    if (!TL) {
      //ierr = DMFEZeroBoundariesSR(dmcoarse,Vc);CHKERRQ(ierr);
      ierr = DMFEZeroFrozenSR(dmcoarse,Vc);CHKERRQ(ierr);
      ierr = DMGetLocalVector(dmcoarse,&Yc);CHKERRQ(ierr);
    } else {
      ierr = DMFEZeroBoundaries(dmcoarse,Vc);CHKERRQ(ierr);
      ierr = DMGetGlobalVector(dmcoarse,&Yc);CHKERRQ(ierr);
    }
    // Correction computed on transition level is only valid on owned parts of 1st SR level subdomain
    ierr = VecCopy(Uc,Yc);CHKERRQ(ierr); // Yc = Uc (store for FAS correction later)
    
    // Calling V-Cycle on next coarser level
    if (!TL) {ierr = MGVCycleSR(op,mg->coarse,presmooths,postsmooths,Vc,Uc);CHKERRQ(ierr);}
    else     {ierr = MGVCycle  (op,mg->coarse,presmooths,postsmooths,Vc,Uc);CHKERRQ(ierr);}
    
    ierr = VecAXPY(Uc,-1.,Yc);CHKERRQ(ierr); // Compute FAS correction Uc = Uc + (-1.*Yc)
    
    if (!TL) {ierr = DMRestoreLocalVector (dmcoarse,&Yc);CHKERRQ(ierr);}
    else     {ierr = DMRestoreGlobalVector(dmcoarse,&Yc);CHKERRQ(ierr);}
  }
  // Interpolate FAS correction to fine grid
  if (!TL) {ierr = DMFEInterpolateSR(dm,Uc,V);CHKERRQ(ierr);}
  else     {
    ierr = OpInterpolate(op,dm,Uc,V);CHKERRQ(ierr);
    ierr = DMFEUpdateGhostSR(dm,U);CHKERRQ(ierr); // Update ghost to have consistent FAS correction in buffers
  }
  
  ierr = VecAXPY(U,1,V);CHKERRQ(ierr); // Add FAS correction to solution: Uf = Uf + V = Uf + I_c^f(Uc-I_f^c(Uf))
  
  if (dmcoarse) {
    if (!TL) {
      ierr = DMRestoreLocalVector(dmcoarse,&Uc);CHKERRQ(ierr);
      ierr = DMRestoreLocalVector(dmcoarse,&Vc);CHKERRQ(ierr);
    } else {
      ierr = DMRestoreGlobalVector(dmcoarse,&Uc);CHKERRQ(ierr);
      ierr = DMRestoreGlobalVector(dmcoarse,&Vc);CHKERRQ(ierr);
    }
  }
  ierr = DMRestoreLocalVector(dm,&V);CHKERRQ(ierr);
  
  if (postsmooths) {
    ierr = KSPSetTolerances(mg->ksp,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT,postsmooths-1);CHKERRQ(ierr);
    ierr = KSPSolve(mg->ksp,B,U);CHKERRQ(ierr);
  }
  
  ierr = PetscLogEventEnd(mg->V_Cycle,mg->dm,B,U,0);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

// Input are LOCAL Vectors B and U
PetscErrorCode MGFCycleSR(Op op,MG mg,PetscInt presmooths,PetscInt postsmooths,Vec B,Vec U)
{
  PetscErrorCode ierr;
  DM dm = mg->dm, dmcoarse;
  PetscInt sr_level;
  Vec Bg, Ug, Uc, Bc;
  
  PetscFunctionBegin;
  ierr = PetscLogStagePush(mg->stage);CHKERRQ(ierr);
  
  ierr = DMGetSRLevel(dm,&sr_level);CHKERRQ(ierr);
  PetscBool TL = (sr_level==1); // Is the next coarser level the transition level?
  if (!(sr_level>0)) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"MGFCycleSR called on non-SR level");
  if (!(mg->coarse)) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"MGFCycleSR cannot compute coarsest level");
  dmcoarse = mg->coarse->dm;
  if (!(dmcoarse) && !TL) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"MGFCycleSR: no process grid coarsening on SR levels");
  
  // Compute norm of B in global space
  if (mg->monitor || mg->diagnostics) {
    ierr = DMGetGlobalVector(dm,&Bg);CHKERRQ(ierr);
    ierr = DMLocalToGlobal_FE_SR(dm,B,INSERT_VALUES,Bg);CHKERRQ(ierr);
    ierr = VecNorm(Bg,NORM_2,&mg->bnorm2);CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(dm,&Bg);CHKERRQ(ierr);
  }
  if (dmcoarse) {
    // Get coarse grid right-hand side vector
    if(!TL) {ierr = DMGetLocalVector (dmcoarse,&Bc);CHKERRQ(ierr);}
    else    {ierr = DMGetGlobalVector(dmcoarse,&Bc);CHKERRQ(ierr);}
    ierr = VecZeroEntries(Bc);CHKERRQ(ierr);
  } else Bc = NULL;
  // Restrict fine grid right-hand side to coarse grid
  if (!TL) {ierr = DMFERestrictSR       (dm,B,Bc);CHKERRQ(ierr);}
  else     {ierr = OpRestrictResidual(op,dm,B,Bc);CHKERRQ(ierr);} // Can do this w/o communication
  
  if (dmcoarse) {
     // Set right-hand side on global boundaries to zero, SR: Set frozen nodes zero
    if (!TL) {
      //ierr = DMFEZeroBoundariesSR(dmcoarse,Bc);CHKERRQ(ierr);
      ierr = DMFEZeroFrozenSR    (dmcoarse,Bc);CHKERRQ(ierr);
    }
    else {
      ierr = DMFEZeroBoundaries(dmcoarse,Bc);CHKERRQ(ierr);
    }
    // Solve coarse grid problem
    if (!TL) {
      ierr = DMGetLocalVector(dmcoarse,&Uc);CHKERRQ(ierr);
      ierr = MGFCycleSR(op,mg->coarse,presmooths,postsmooths,Bc,Uc);CHKERRQ(ierr);
    }
    else {
      ierr = DMGetGlobalVector(dmcoarse,&Uc);CHKERRQ(ierr);
      ierr = MGFCycle(op,mg->coarse,presmooths,postsmooths,Bc,Uc);CHKERRQ(ierr);
    }
  } else Uc = NULL;
  
  // FMG Interpolation must also fill SR buffer
  // Interpolate solution to fine grid (initial guess)
  if (!TL) {ierr = DMFEInterpolateSR(dm,Uc,U);CHKERRQ(ierr);}
  else     {ierr = OpInterpolate (op,dm,Uc,U);CHKERRQ(ierr);}
  
  if (dmcoarse) {
    if (!TL) {
      ierr = DMRestoreLocalVector(dmcoarse,&Uc);CHKERRQ(ierr);
      ierr = DMRestoreLocalVector(dmcoarse,&Bc);CHKERRQ(ierr);
    } else {
      ierr = DMRestoreGlobalVector(dmcoarse,&Uc);CHKERRQ(ierr);
      ierr = DMRestoreGlobalVector(dmcoarse,&Bc);CHKERRQ(ierr);
    }
  }
  // Calling V-Cycle from SR level
  ierr = MGVCycleSR(op,mg,presmooths,postsmooths,B,U);CHKERRQ(ierr);
  
  // MG monitor
  ierr = PetscLogStagePop();CHKERRQ(ierr);
  if (mg->monitor || mg->diagnostics) {
    ierr = DMGetGlobalVector(dm,&Bg);CHKERRQ(ierr);
    ierr = DMGetGlobalVector(dm,&Ug);CHKERRQ(ierr);
    ierr = DMLocalToGlobal_FE_SR(dm,B,INSERT_VALUES,Bg);
    ierr = DMLocalToGlobal_FE_SR(dm,U,INSERT_VALUES,Ug);
    ierr = MGRecordDiagnostics(op,mg,Bg,Ug);CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(dm,&Bg);CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(dm,&Ug);CHKERRQ(ierr);
  }
  
  PetscFunctionReturn(0);
}

static PetscErrorCode OptionsCheckSR(Options opt,Op op)
{
  PetscErrorCode ierr;
  PetscBool flg;
  PetscFunctionBegin;
  // (1) Check if operator is poisson1
  ierr = OpIsOpname(op,"poisson1",&flg);CHKERRQ(ierr);
  if (!flg) {SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Segmental refinement supports -op_type %s only","poisson1");}
  // (2) Unset SR if sr_levels or sr_max_buf is non-positive
  if(!(opt->sr_levels>0)||!(opt->sr_max_buf>0))
  {SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Parameter sr_levels or sr_max_buf is non-positive");}
  // (3) Check if sr_levels is valid
  // GridCreate: max levels (no change in process grid on SR levels)
  // (4) Check if sr_max_buf is valid
  // GridCreate: max size of buffer s.t. coarse_known_max 27 is sufficient
  // DMCreateFE: max size of buffer s.t. SR buffer does not span beyond the neighbor's subdomain
  PetscFunctionReturn(0);
}

PetscErrorCode RunFMG()
{
  PetscErrorCode ierr;
  Grid grid;
  Options opt;
  Op op;
  PetscInt fedegree,dof,nlevels;
  DM dm;
  Vec U0,U,F,Ul,Fl,Y;
  MG mg;
  PetscReal normU0,normError,normF,normResid;

  PetscFunctionBegin;
  ierr = OpCreateFromOptions(PETSC_COMM_WORLD,&op);CHKERRQ(ierr);
  ierr = OpGetFEDegree(op,&fedegree);CHKERRQ(ierr);
  ierr = OpGetDof(op,&dof);CHKERRQ(ierr);
  // Parse -sr_levels num, -sr_max_buf num
  ierr = OptionsParse("Finite Element FAS FMG solver",&opt);CHKERRQ(ierr);
  // Verify that arguments are valid for SR
  if (opt->sr_levels || opt->sr_max_buf) {ierr = OptionsCheckSR(opt,op);CHKERRQ(ierr);}
  // Grid stores SR meta data: (sr_level, sr_buf)
  ierr = GridCreate(PETSC_COMM_WORLD,opt->M,opt->p,opt->cmax,opt->sr_levels,opt->sr_max_buf,&grid);CHKERRQ(ierr);
  ierr = GridGetNumLevels(grid,&nlevels);CHKERRQ(ierr);
  // FE stores SR meta data: (lmsrl, lmsrh)
  ierr = DMCreateFE(grid,fedegree,dof,&dm);CHKERRQ(ierr);
  ierr = DMFESetUniformCoordinates(dm,opt->L);CHKERRQ(ierr);
  if (opt->coord_distort) {ierr = DMCoordDistort(dm,opt->L);CHKERRQ(ierr);}

  ierr = DMCreateGlobalVector(dm,&U0);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm,&F);CHKERRQ(ierr);
  ierr = OpForcing(op,dm,F);CHKERRQ(ierr);
  ierr = OpSolution(op,dm,U0);CHKERRQ(ierr);
  ierr = VecNorm(U0,NORM_2,&normU0);CHKERRQ(ierr);
  ierr = VecNorm(F,NORM_2,&normF);CHKERRQ(ierr);
  
  if (opt->sr_levels) {
    // If SR, then create local vectors Ul and Fl, and do GlobalToLocal (fill SR buffers):
    // F->Fl and U->Ul, and pass "overlapping" vectors Fl and Ul to F- or V-Cycle
    ierr = DMCreateLocalVector(dm,&U);CHKERRQ(ierr);
    ierr = DMCreateLocalVector(dm,&Fl);CHKERRQ(ierr);
    ierr = DMGlobalToLocal_FE_SR(dm,F,Fl);CHKERRQ(ierr);
    ierr = VecDestroy(&F);CHKERRQ(ierr);
    F = Fl;
  } else {
    ierr = DMCreateGlobalVector(dm,&U);CHKERRQ(ierr);
  }
  
  ierr = MGCreate(op,dm,nlevels,&mg);CHKERRQ(ierr);
  ierr = MGSetProfiling(mg,PETSC_TRUE);CHKERRQ(ierr);
  
  if (opt->sr_levels) {ierr = MGFCycleSR(op,mg,opt->smooth[0],opt->smooth[1],F,U);CHKERRQ(ierr);}
  else                {ierr = MGFCycle  (op,mg,opt->smooth[0],opt->smooth[1],F,U);CHKERRQ(ierr);}
  
  if (opt->sr_levels) {
    // When SR is done, do LocalToGlobal: Fl->F and Ul->U, and destroy local vectors Ul and Fl
    ierr = DMCreateGlobalVector(dm,&Ul);CHKERRQ(ierr);
    ierr = DMCreateGlobalVector(dm,&Fl);CHKERRQ(ierr);
    ierr = DMLocalToGlobal_FE_SR(dm,U,INSERT_VALUES,Ul);CHKERRQ(ierr);
    ierr = DMLocalToGlobal_FE_SR(dm,F,INSERT_VALUES,Fl);CHKERRQ(ierr);
    ierr = VecDestroy(&U);CHKERRQ(ierr);
    ierr = VecDestroy(&F);CHKERRQ(ierr);
    U = Ul; F = Fl;
  }
  
  ierr = DMGetGlobalVector(dm,&Y);CHKERRQ(ierr);
  ierr = VecCopy(U,Y);CHKERRQ(ierr);
  ierr = VecAXPY(Y,-1,U0);CHKERRQ(ierr);
  ierr = VecNorm(Y,NORM_2,&normError);CHKERRQ(ierr);
  ierr = OpApply(op,dm,U,Y);CHKERRQ(ierr);
  ierr = VecAYPX(Y,-1,F);CHKERRQ(ierr);
  ierr = VecNorm(Y,NORM_2,&normResid);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"%s(%D,%D): |e|_2/|u|_2 %8.2e  |r|_2/|f|_2 %8.2e\n","F",opt->smooth[0],opt->smooth[1],(double)(normError/normU0),(double)(normResid/normF));CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dm,&Y);CHKERRQ(ierr);
  
  ierr = MGDestroy(&mg);CHKERRQ(ierr);
  ierr = VecDestroy(&U0);CHKERRQ(ierr);
  ierr = VecDestroy(&U);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  ierr = GridDestroy(&grid);CHKERRQ(ierr);
  ierr = OpDestroy(&op);CHKERRQ(ierr);
  ierr = PetscFree(opt);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
