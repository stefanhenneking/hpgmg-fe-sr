#!/bin/bash -l

#SBATCH -p debug
#SBATCH -N 32
#SBATCH -t 00:10:00
#SBATCH -J hpgmg-fe-sr

# Variables
JOBID=${SLURM_JOBID}
NODES=${SLURM_JOB_NUM_NODES}
CORES=24*32
PRE=3
POST=1
S=4
Q=4
echo "N =" ${NODES}
echo "n =" ${CORES}
echo "Nodes:" ${SLURM_JOB_NODELIST}
echo "JobId:" ${JOBID}

date

Mx=1024
My=1024
Mz=1024
px=4
py=4
pz=4
((p=px*py*pz))
echo "M = [" ${Mx} ${My} ${Mz} "]"
echo "p = [" ${px} ${py} ${pz} "]"
srun -n ${p} ./build/bin/hpgmg-fe fmg -mg_monitor -p ${px},${py},${pz} -M ${Mx},${My},${Mz} -op_type poisson1 -smooth ${PRE},${POST} -sr_levels ${S} -sr_max_buf ${Q} >& n0000${p}_M00${Mx}_S${S}.Q${Q}.${JOBID}.out

date
# eof
