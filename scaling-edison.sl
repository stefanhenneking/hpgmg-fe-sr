#!/bin/bash -l

#SBATCH -p regular
#SBATCH -N 683
#SBATCH -t 00:08:00
#SBATCH -J hpgmg-fe-sr

# Variables
JOBID=${SLURM_JOBID}
NODES=${SLURM_JOB_NUM_NODES}
CORES=24*SLURM_JOB_NUM_NODES
PRE=3
POST=1
SAMPLES=8
S=0
Q=0
echo "N =" ${NODES}
echo "n =" ${CORES}
echo "Nodes:" ${SLURM_JOB_NODELIST}
echo "JobId:" ${JOBID}

date
mp=32
((m=mp*2))
((lmin=mp*mp*mp))
((lmax=lmin+1))
p=1
#srun -n ${p} ./build/bin/hpgmg-fe sample -repeat ${SAMPLES} -op_type poisson1 -smooth ${PRE},${POST} -local ${lmin},${lmax} -sr_levels ${S} -sr_max_buf ${Q} -log_summary >& n00000${p}_mp${mp}_S${S}.Q${Q}.$JOBID.out

date
p=2
#srun -n ${p} ./build/bin/hpgmg-fe sample -repeat ${SAMPLES} -op_type poisson1 -smooth ${PRE},${POST} -local ${lmin},${lmax} -sr_levels ${S} -sr_max_buf ${Q} -log_summary >& n00000${p}_mp${mp}_S${S}.Q${Q}.$JOBID.out

date
p=4
#srun -n ${p} ./build/bin/hpgmg-fe sample -repeat ${SAMPLES} -op_type poisson1 -smooth ${PRE},${POST} -local ${lmin},${lmax} -sr_levels ${S} -sr_max_buf ${Q} -log_summary >& n00000${p}_mp${mp}_S${S}.Q${Q}.$JOBID.out

date
p=8
#srun -n ${p} ./build/bin/hpgmg-fe sample -repeat ${SAMPLES} -op_type poisson1 -smooth ${PRE},${POST} -local ${lmin},${lmax} -sr_levels ${S} -sr_max_buf ${Q} -log_summary >& n00000${p}_mp${mp}_S${S}.Q${Q}.$JOBID.out

date
p=16
#srun -n ${p} ./build/bin/hpgmg-fe sample -repeat ${SAMPLES} -op_type poisson1 -smooth ${PRE},${POST} -local ${lmin},${lmax} -sr_levels ${S} -sr_max_buf ${Q} -log_summary >& n00000${p}_mp${mp}_S${S}.Q${Q}.$JOBID.out

date
p=32
#srun -n ${p} ./build/bin/hpgmg-fe sample -repeat ${SAMPLES} -op_type poisson1 -smooth ${PRE},${POST} -local ${lmin},${lmax} -sr_levels ${S} -sr_max_buf ${Q} -log_summary >& n00000${p}_mp${mp}_S${S}.Q${Q}.$JOBID.out

date
p=64
srun -n ${p} ./build/bin/hpgmg-fe sample -repeat ${SAMPLES} -op_type poisson1 -smooth ${PRE},${POST} -local ${lmin},${lmax} -sr_levels ${S} -sr_max_buf ${Q} -log_summary >& n0000${p}_mp${mp}_S${S}.Q${Q}.$JOBID.out

date
p=128
srun -n ${p} ./build/bin/hpgmg-fe sample -repeat ${SAMPLES} -op_type poisson1 -smooth ${PRE},${POST} -local ${lmin},${lmax} -sr_levels ${S} -sr_max_buf ${Q} -log_summary >& n000${p}_mp${mp}_S${S}.Q${Q}.$JOBID.out

date
p=256
srun -n ${p} ./build/bin/hpgmg-fe sample -repeat ${SAMPLES} -op_type poisson1 -smooth ${PRE},${POST} -local ${lmin},${lmax} -sr_levels ${S} -sr_max_buf ${Q} -log_summary >& n00000${p}_mp${mp}_S${S}.Q${Q}.$JOBID.out

date
p=512
srun -n ${p} ./build/bin/hpgmg-fe sample -repeat ${SAMPLES} -op_type poisson1 -smooth ${PRE},${POST} -local ${lmin},${lmax} -sr_levels ${S} -sr_max_buf ${Q} -log_summary >& n00000${p}_mp${mp}_S${S}.Q${Q}.$JOBID.out

date
p=1024
srun -n ${p} ./build/bin/hpgmg-fe sample -repeat ${SAMPLES} -op_type poisson1 -smooth ${PRE},${POST} -local ${lmin},${lmax} -sr_levels ${S} -sr_max_buf ${Q} -log_summary >& n00000${p}_mp${mp}_S${S}.Q${Q}.$JOBID.out

date
p=2048
srun -n ${p} ./build/bin/hpgmg-fe sample -repeat ${SAMPLES} -op_type poisson1 -smooth ${PRE},${POST} -local ${lmin},${lmax} -sr_levels ${S} -sr_max_buf ${Q} -log_summary >& n00000${p}_mp${mp}_S${S}.Q${Q}.$JOBID.out

date
p=4096
srun -n ${p} ./build/bin/hpgmg-fe sample -repeat ${SAMPLES} -op_type poisson1 -smooth ${PRE},${POST} -local ${lmin},${lmax} -sr_levels ${S} -sr_max_buf ${Q} -log_summary >& n00000${p}_mp${mp}_S${S}.Q${Q}.$JOBID.out

date
p=8192
srun -n ${p} ./build/bin/hpgmg-fe sample -repeat ${SAMPLES} -op_type poisson1 -smooth ${PRE},${POST} -local ${lmin},${lmax} -sr_levels ${S} -sr_max_buf ${Q} -log_summary >& n00000${p}_mp${mp}_S${S}.Q${Q}.$JOBID.out

date
p=16384
srun -n ${p} ./build/bin/hpgmg-fe sample -repeat ${SAMPLES} -op_type poisson1 -smooth ${PRE},${POST} -local ${lmin},${lmax} -sr_levels ${S} -sr_max_buf ${Q} -log_summary >& n00000${p}_mp${mp}_S${S}.Q${Q}.$JOBID.out

date

# eof
